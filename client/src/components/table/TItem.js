import React, { Component } from 'react';
import Moment from 'react-moment';


export default class TItem extends Component {

        
        componentWillMount() {
        }
        
        render () {
                const event = this.props.event;
                
                this.title = event.title;
                this.start = event.start;
                this.end = event.end;
                this.isActive = event.isActive;

                return (
                        <div className={"row align-items-center evento item" + ( ( this.isActive || false ) ? " actual" : " " ) }>
                                <div className="col-8">
                                        <b className="item-titolo">{this.title}</b>
                                </div>
                                <div className="col-2 text-center">
                                        <b className="item-inizio"><Moment format="HH:mm">{this.start}</Moment></b>
                                </div>
                                <div className="col-2 text-center">
                                        <b className="item-fine"><Moment format="HH:mm">{this.end}</Moment></b>
                                </div>
                        </div>
                )
        }
}