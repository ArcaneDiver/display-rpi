import React, { Component } from 'react';

export default class THead extends Component {
        render() {
                return (
                        <div className="row evento text-center"> 
                                <div className="col-8 col-offset-1">
                                        <b>Eventi</b>
                                </div>
                                <div className="col-2">
                                        <b>Dalle</b>
                                </div>
                                <div className="col-2">
                                        <b>Alle</b>
                                </div>
                        </div>
                )
        }
}