import React, { Component } from 'react';
import moment from 'moment';

import TItem from './TItem';

export default class TItems extends Component {
        render () {
                const items = [];

                for ( var i = 0; i < this.props.data.length; i++ ) {
                        
                        if ( this.props.data[i].endStampOff > moment().set({hour:0,minute:0,second:0,millisecond:0}).format('X') )
                                items.push(<TItem key = {i} id = {i} event = {this.props.data[i]} callback={this.props.callback}/>)
                }
                if ( items.length === 0) {
                        return ( <div className="alert alert-danger text-center mt-5" >Nessun evento disponibile</div>)
                }
                return (
                        <>{ items }</>
                )
        }
}