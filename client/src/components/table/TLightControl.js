import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

export default class TLightControl extends Component {
        constructor(props) {
                super(props);

                this.handleClick = this.handleClick.bind(this);
                this.state = {
                  return: 0
                }  
              
        }
        componentDidMount() {
                
                //Dopo 30 secondi torna alla pagina home
                this.homePage = setTimeout(() => {
                        this.setState( state => state.return = 1);
                }, 30 * 1000);
        }
        componentWillUnmount() {
                clearTimeout ( this.homePage );
        }
       
        handleClick(data){

                // id del bottone
                const type = data.target.id;

                // Prendo il socket
                const socket = this.props.socket;

                //Invio al socket in base al tipo
                switch (type) {
                        case 'on':
                                this.props.events.map( e => {
                                        
                                        if ( e.isActivable ) {
                                                
                                                // Comunico che diventa on
                                                socket.emit('rele', { type });
                                                this.props.callback(type);
                                        }
                                        return e;
                                })                               
                                break;
                        case 'off':
                                socket.emit('rele', { type });
                                this.props.callback(type);
                                break;
                        case 'force':
                                socket.emit('rele', { type });
                                this.props.callback(type);

                                break;
                        default:
                                break;
                }
                this.setState({
                        return: 1
                });
                        
        }

        render() {
                if ( this.state.return ) {
                        return (
                                <Redirect to={{
                                        pathname: '/'
                                }}/>
                        )
                }
                
                return ( 
                        <div id="display-extra">
                                <div className="row justify-content-center">
                                        <div className="col-12 text-center">
                                                <b>Controllo luce</b>
                                        </div>
                                </div>
                                <div className="row justify-content-center">
                                        <div className="col-6">
                                                <button id="on"
                                                        className={`btn ${this.props.location.state.isOneEventActive ? `btn-success` : `btn-secondary`} btn-block`} onClick={(e)=>this.handleClick(e)}>
                                                        Accendi
                                                </button>
                                        </div>
                                        <div className="col-6">
                                                <button id="off"
                                                        className="btn btn-danger btn-block" onClick={(e)=>this.handleClick(e)}>
                                                        Spegni
                                                </button>
                                        </div>
                                </div>
                                {
                                        this.props.location.state.permission === 'advanced' 
                                                ? <div className="row justify-content-center">
                                                        <div className="col-6">
                                                                <button id="force"
                                                                        className="btn btn-warning btn-block" onClick={(e)=>this.handleClick(e)}>
                                                                        Forza accensione
                                                                </button>
                                                        </div>
                                                </div>
                                                : ''       
                                }
                                <div className="row justify-content-center">
                                        <div className="col-6">
                                                <button id="back"
                                                        className="btn btn-secondary btn-block" onClick={(e)=>this.handleClick(e)}>
                                                        <i className="fa fa-arrow-left"></i> Eventi
                                                </button>
                                        </div>
                                </div>
                        </div>
                )
        }
}