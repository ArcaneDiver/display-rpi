import React, { Component } from 'react';

export default class LightState extends Component {
        render() {
                const btnState = (this.props.state === 'on') ? 'btn-success' : ( (this.props.state === 'off') ? 'btn-danger' : 'btn-warning');
                return(
                        <div id="lbl-light-status" className={"btn " +  btnState  + " btn-block"}>
                                <i id="icn-light-status" className="fa fa-lightbulb-o"></i>
                        </div>
                )
        }
}