import React, { Component } from 'react';
import * as io from 'socket.io-client';
import { Route, withRouter } from 'react-router-dom';
import moment from 'moment';

import Header from './Header';
import Time from './Time';
import Temperature from './Temperature';
import LightState from './LightState';
import Table from './Table';
import THead from './table/THead';
import TBody from './table/TBody';
import TLightControl from './table/TLightControl';
import TItems from './table/TItems';

console.log(io);

const socket = io();

class App extends Component {
        constructor(props) {

                super(props);

                this.changeLightState = this.changeLightState.bind(this);
                
                
                this.state = {
                        temperature: 'n.d.',
                        lightState: 'off',
                        activeTimeout: null,
                        eventData: false,
                        connected: true
                };
                
        }
        
        rfidData = async data => {
                
                // Reindizzo con i permessi
                this.props.history.push({
                        pathname: '/light',
                        state: {
                                permission: data,
                                isOneEventActive: this.state.isOneEventActive
                        }
                });


        }

        processEvents = async payload => {
                      
                const res = payload.events;
                const OFFSET = payload.offset
                
                console.log(' [EVENTS] Richiesta avvenuta con successo | RES =>', res);
                        
                
                let isOneEventActive = false;
                res.map ( ( e, index ) => {

                        const actualStamp = parseInt(moment().format('X'));
                        
                        // Salvo lo stamp con gli offset
                        e.startStampOff = parseInt(moment( e.start ).format('X')) + ( OFFSET.offsetOn * 60 );
                        e.endStampOff = parseInt(moment( e.end ).format('X')) + ( OFFSET.offsetOff * 60 );

                        // Salvo lo stamp senza gli offset
                        e.startStamp = parseInt(moment( e.start ).format('X'));
                        e.endStamp = parseInt(moment( e.end ).format('X'));

                        // Imposto lo stato degli eventi
                        e.isActive = ( e.startStampOff <= actualStamp && actualStamp <= e.endStampOff ) ? true : false;
                        e.isActivable = ( e.startStampOff <= actualStamp && actualStamp <= e.endStamp ) ? true : false;
                        
                        // Guardo se esiste un evento attivo
                        if( e.isActive ) isOneEventActive = true;

                        return e;
                })

                
                // Se la luce era 'on' e non c'è più nessun evento attivo allo spengo
                if ( !isOneEventActive && this.state.lightState === 'on') {
                        socket.emit('rele', { type: 'off' });
                        this.setState({ lightState: 'off'});
                }
                
                
                // Imposto gli eventi nello stato
                this.setState(state=> {
                        state.eventData = res;
                        state.isOneEventActive = isOneEventActive;
                })
        }

        processTemperature = async res => {

                console.log(' [TEMPERATURE] Richiesta avvenuta con successo | RES =>', res);

                // Comunico lo stato
                this.setState({
                        temperature: res
                })
                
        }
        changeLightState = type => {

                // Comunico lo stato della luce
                this.setState({
                        lightState: type
                })
        }
        componentDidMount = async () => {    
                // Ottengo l'ultimo stato dal backend
                socket.on('lastState', type => {
                        console.log(' [LAST_STATE]', type);
                        this.setState({ lightState: type })
                        socket.removeListener('lastState');
                });
                
                socket.on( 'events', data => {
                        this.processEvents( data );
                })
                
                socket.on( 'temperature', data => {
                        this.processTemperature( data );
                })

                // Ottengo lo status del badge
                socket.on('RFID', data => this.rfidData(data));
                
                // Ottengo lo stato della connessione
                socket.on('net', data => this.setState({ connected: data }));


                // Disabilito il right click
                document.addEventListener('contextmenu', e => {
                        e.preventDefault();     
                });
        }

        render = () => (
                <div className="container-fluid text-big">
                        <Header net = {this.state.connected}/>
                        <div className="row">
                                <div className="col-3 text-center">
                                        
                                        <Time />
                                        
                                        <br/>

                                        <Temperature value={this.state.temperature} />
                                        
                                        <br/>
                                        
                                        <LightState state = {this.state.lightState}/>
                                
                                </div>
                                <div className="col-9"> 
                                        <Table >
                                                <Route exact path="/" render={
                                                        (props)=>{
                                                                return (
                                                                        <>
                                                                                <THead {...props}/>
                                                                                <TBody {...props}>
                                                                                        <TItems data = {this.state.eventData}/>
                                                                                </TBody>
                                                                        </>
                                                                );
                                                        }
                                                }/>
                                                <Route path="/light" render = {
                                                        props =>{ 
                                                                return (
                                                                        <TLightControl 
                                                                                socket={socket}
                                                                                events={this.state.eventData}
                                                                                callback={this.changeLightState} 
                                                                                {...props}
                                                                        />
                                                                ) 
                                                        }
                                                } 
                                                />
                                        </Table>
                                </div>
                        </div>
                </div>
        );
        
}

export default withRouter(App);