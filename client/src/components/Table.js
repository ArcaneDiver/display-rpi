import React, { Component } from 'react';

export default class Table extends Component {
        constructor(props) {
                super(props);

                // create a ref to store the textInput DOM element
                this.refTest = React.createRef();
                
        }
            
      
        render() {
                
                return (
                        <div id="display-eventi">
                                {this.props.children}     
                        </div>
                )
        }
}