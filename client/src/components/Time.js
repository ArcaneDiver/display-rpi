import React, { Component } from 'react';

import Clock  from 'react-clock';
import Timestamp from 'react-live-clock';


export default class Time extends Component {

        state = {
                date: new Date()
        }
             
        componentDidMount() {
                setInterval(() => this.setState({ date: new Date() }), 1000);
        }
        render() {
                var date = new Date().toLocaleDateString('it', {
                        weekday: 'long'
                });
                
                date = date.charAt(0).toUpperCase() + date.slice(1)

                return (
                        <>
                                <b >
                                        <Timestamp format={'DD/MM/YY'} ticking={true} timezone={'Europe/Rome'} />
                                        <br />
                                        {date}

                                </b>
                                <Clock value = {this.state.date} className="clock_center" />
                        </>
                )
        }
}