import React , { Component } from 'react';
 

export default class Header extends Component {
        
        componentDidMount() {
        }
        
        render() {
                return (
                        <div className="row align-items-center">
                                <div className="col-3">
                                        <img id="logoBearzi" src={"./logoBearzi.png"} alt="Logo Bearzi" width="100%"
                                        height="auto"/>
                                </div>
                                <div className="col-9 text-center">
                                       <b id="locale">Palestra</b>
                                
                                                <i id="globe" className={"fa fa-globe float-right " + ( this.props.net ? "text-success" : "text-danger")}></i>
                                        
                                </div>
                        </div>
                )
        }
}