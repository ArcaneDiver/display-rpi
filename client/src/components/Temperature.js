import React, { Component } from 'react';

export default class Temperature extends Component {
        render() {
                return(
                        <div id="display-temperatura"
                                className="btn btn-info btn-block">
                                <i className="fa fa-thermometer-half "></i> {this.props.value}  
                        </div>
                )
        }
}