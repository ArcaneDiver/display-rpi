const fs = require('fs');
const path = require('path');

module.exports = (req, res) => {
        var cache = [];
         
        const ip = req.hostname;

        //Ottengo i dati
        const query = req.query;
        
        if ( "eventi" in query ) {
                const jsonToSend = JSON.parse( fs.readFileSync(path.resolve(__dirname, '../config.json')));
                res.status(200).send(jsonToSend);
                //res.jsonp(jsonToSend);
        } else if ( "temperatura" in query ) {
                res.jsonp((Math.random() * (30 - 20) + 20).toFixed(1) + '°C');
        } else {
                //console.log('->' + fs.readFileSync( path.resolve(__dirname, './badge') ).toString() + '<-');
                res.jsonp(fs.readFileSync( path.resolve(__dirname, './badge') ).toString());
        }

        res.end();
};
