const path = require('path');

// import il .env
require('dotenv').config({
        path: path.resolve(__dirname, '../.env')
});

const env = process.env;

module.exports = {
        OFFSET_ON: parseInt(env.OFFSET_ON),
        OFFSET_OFF: parseInt(env.OFFSET_OFF),
        TIME_RANGE: parseInt(env.TIME_RANGE),
        URL: env.URL,
        PIN: parseInt(env.PIN)
}; 
