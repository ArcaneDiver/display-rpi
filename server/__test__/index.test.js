// Moduli per il testing
const assert = require('assert');
const Mocha = require('mocha');
const addContext = require('mochawesome/addContext');

// Moduli
const http = require('http');
const Gpio = require('onoff').Gpio;
const axios = require('axios');
const puppeteer = require('puppeteer-core');

// Setto l'env
process.env.TEST = true;

// Setto una porta
const port = 6756;

// Importo il server
const server = require('../App');

// Importo le env processate
const { PIN, URL } = require('../config/index');

describe('Display Raspberry', function () {
        
        let rele;
        
        describe('Server', function () {

                it('Start correctly', function () {
                        server.listen(port);
                        addContext(this, 'ciao');

                });
        
                
                it('Serve the REACT page', function (done) {
                        http.get('http://localhost:' + port, res => {
                                done(!assert.equal(200, res.statusCode) ? null : new Error(' Request failed. Status Code: ' + res.statusCode));
                        });
                })
                
        })
        
        describe('Gpio', function () {
        
                it('Pin initialized', function (done) {
                        try {
                                
                                rele = new Gpio(PIN, 'out');
                                done();

                        } catch (err) {
                                
                                done(new Error(' [ERROR] => ' + err.code));

                                
                        }
                })

                it('Relé opened', function (done) {
                        
                        try {

                                rele.writeSync(1);
                                
                                if(rele.readSync()) done();

                        } catch(err) {

                               done(new Error(' [ERROR] => ' + err.code));

                        }

                })

                it('Relé closed', function (done) {

                        try {

                                rele.writeSync(0);

                                if(!rele.readSync()) done();

                        } catch(err) {

                                done(new Error(' [ERROR] => ' + err.code));
                        }

                })
                
                it('Relé exported', function (done) {

                        try {

                                rele.unexport();

                                done();

                        } catch(err) {

                                done(new Error(' [ERROR] => ' + err.code));

                        }
                })
        })

        describe('API Call', function () {
                it('Events Call', function (done) {
                        
                        const _this = this;
                        
                        axios.get( URL, {
                                timeout: 1000,
                                params: {
                                        eventi: 1
                                }
                        }).then( function (res) {

                                
                                addContext(_this, 
                                        {
                                                title: 'Events Call Results',
                                                value: res.data
                                        }
                                ); 
                                
                                done()
                        })
                        .catch( function (err) {
                                
                                done(new Error(err));
                                
                        })
                       

                })
                it('Temperature Call',  function (done) {
                        
                        const _this = this;

                        axios.get( URL, {
                                timeout: 1000,
                                params: {
                                        temperatura: 1
                                }
                        }).then( function (res) {

                                addContext(_this, 
                                        {
                                                title: 'Temperature Call Results',
                                                value: res.data
                                        }
                                ); 

                                done()
                        })
                        .catch( function (err) {
                                
                                done(new Error(err));
                                
                        })

                       

                })
                it('Badge Call',  function (done) {
                        const _this = this;

                        axios.get( URL, {
                                timeout: 1000,
                                params: {
                                        codice: '010EB9093F'
                                }
                        }).then( function (res) {
                                
                                addContext(_this,
                                        {
                                                title: 'Badge Call Results',
                                                value: res.data
                                        }
                                ); 
                                
                                done()
                        })
                        .catch( function (err) {
                                
                                done(new Error(err));
                                
                        })

                       

                })
        })

        describe('Chromium', function () {

                let browser = false;

                it('Create new browser instance', function (done) {
                        

                        const args = [];

                        args.push('--kiosk');
                        args.push('--disable-infobars');
                        args.push('--disable-pinch');
                        args.push('--disable-popup-blocking');
                        args.push('--no-sandbox');
                        args.push('–-overscroll-history-navigation=0');

                        // Lanch pupeteer headless with custom arguments
                        puppeteer.launch({
                                headless: true,
                                args,
                                executablePath: '/usr/bin/chromium-browser'
                        })
                        .then( function (browserInstance) {
                                browser = browserInstance
                                done();
                        })
                        .catch( function (err) {done( new Error(err.message) )});

                })

                it('Navigate to React Page', function (done) {

                        if (!browser){
                                done(new Error('Browser Instance doesn\'t not exist'));
                        }

                        browser.newPage()
                        .then( page => {
                               
                                page.goto(`localhost:${port}`)
                                .then( function () {
                                        done();
                                })
                                .catch( function (err) {
                                        done( new Error(err.message) );
                                })
                        })
                        .catch( function (err) {
                                done( new Error(err.message) );
                        })
        
                })
        })
        
        after( function () {
                server.close();
                
        })
})