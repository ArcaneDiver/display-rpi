const app = require('express')();
const http = require('http').createServer(app);

require('./config');

// Import le route
const routes = require('./routes');


// Middlewares
app.use(require('express').static('build'));


// Routes
app.use(routes);

if(!process.env.TEST) 
        // Init the utils
        new (require('./utils'))(http).init();




module.exports = http;