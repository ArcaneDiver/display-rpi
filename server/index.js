const server = require('./App');

// La porta
const port = process.env.PORT || 3001;

// Ascolto la porta
server.listen(port, '127.0.0.1', ()=>{
        console.log('Server Side on', port);
})