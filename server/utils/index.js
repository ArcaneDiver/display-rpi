const fs = require('fs');
const path = require('path');

const puppeteer = require('puppeteer-core');

const RFIDReader = require('./rfid');
const { initRele, fetch, requestRFID } = require('./functions');


// Import le config di .env
const { TIME_RANGE } = require('../config/index');

module.exports = class Utils {

        constructor (http) {
                this.io = require('socket.io')(http);
        }

        init () {

                const { rele, lastState } = initRele();
        
                this.rele = rele, this.lastState = lastState;

                this.initIO();

                this.initBrowser();

                // Ascolto gli eventi generati dall'rfid
                RFIDReader.on('dataRFID', data => requestRFID(data, this.io));
        }

        initIO () {

                // Socket
                this.io.on('connection', async socket => {
                        

                        try { this.lastEvents = JSON.parse(fs.readFileSync( path.resolve(__dirname, 'temp/lastEvents.json') )) }
                        catch (err) { console.log(' [ERR]', err) }
                        

                        try { this.lastState = fs.readFileSync( path.resolve(__dirname, 'temp/lastState.txt') ).toString() }
                        catch (err) { this.lastState = this.lastState || false }

                        
                        // Passo l'ultimo stato
                        if ( this.lastState ) socket.emit('lastState', this.lastState);
                        

                        // Ascolto per l'evento del rele
                        socket.on('rele', state => {
                                             
                                
                                switch (state.type) {
                                        case 'on':
                                                this.rele.writeSync(1);
                                                break;
                                        case 'off':
                                                this.rele.writeSync(0);
                                                break;
                                        case 'force':
                                                this.rele.writeSync(1);
                                                break;
                                }

                                
                                fs.writeFileSync( path.resolve(__dirname, 'temp/lastState.txt'), state.type);
                        });
                        
                        // Intervallo per fetch degli eventi e della temperatura
                        setInterval( fetch, TIME_RANGE, socket, this.lastEvents);
                        fetch(socket, this.lastEvents ? this.lastEvents : false);
                        
                        
                        this.initUnexport();
                })
        }

        initUnexport () {

                process.on('SIGINT', () => {
                        this.rele.unexport();
                })
                
                process.on('SIGHUP', () => {
                        this.rele.unexport();
                })
        }

        async initBrowser () {
                
                const args = [];

                // ARGUMENTS
                args.push('--kiosk');
                args.push('--disable-infobars');
                args.push('--disable-pinch');
                args.push('--disable-popup-blocking');
                args.push('--no-sandbox');
                args.push('–-overscroll-history-navigation=0');
                
                if( process.env.DEBUG) {
                        args.push('--remote-debugging-port=9222');
                        
                        console.log('DEBUGGING Port: 9222');
                }
                


                // Lanch pupeteer with custom arguments
                const browser = await puppeteer.launch({
                        headless: false,
                        args,
                        executablePath: '/usr/bin/chromium-browser'
                });
                

                const page = await browser.newPage();
                
                await page.goto('localhost:' + process.env.PORT);
        }
}
