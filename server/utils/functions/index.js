const Gpio = require('onoff').Gpio;
const axios = require('axios');
const fs = require('fs');
const path = require('path');

const { URL, OFFSET_ON, OFFSET_OFF, PIN } = require('../../config/index');

module.exports = {
        fetch: async ( socket, lastEvents ) => {
                        
                try {
                        {
                                // Eventi
                                const res = await axios.get( URL, {
                                        timeout: 5000,
                                        params: {
                                                eventi: 1
                                        }
                                })

                                if( process.env.DEBUG) {

                                        console.log(res.data);
                                }
                                        
                                socket.emit('events', { events: res.data , offset: { offsetOn: OFFSET_ON, offsetOff: OFFSET_OFF } } );

                                fs.writeFile( path.resolve(__dirname, '../temp/lastEvents.json'), JSON.stringify(res.data, null, 2), () => {})
                        
                        }
                        {
                                // Temperatura
                                const res = await axios.get( URL, {
                                        timeout: 5000,
                                        params: {
                                                temperatura: 1
                                        }
                                })
                                
                                if( process.env.DEBUG) {

                                        console.log(res.data);
                                }

                                socket.emit('temperature', res.data);
                        }
                        socket.emit('net', true);


                } catch ( err ) {
                        console.log(err.message);
                        // Se ottengo un errore
                        if ( lastEvents ) socket.emit('events', { events: lastEvents , offset: { offsetOn: OFFSET_ON, offsetOff: OFFSET_OFF } });
                        socket.emit('net', false);
                }
        },



        requestRFID: async ( data, socket ) => {

                try {

                        // Richiedo lo stato del badge
                        const res = await axios.get( URL, {
                                timeout: 2000,
                                params: {
                                        codice: data.badge
                                }
                        })

                        if(process.env.DEBUG) {
                                
                                console.log(res.data, `|${data.badge}|`);
                        }
                                

                        if((res.data === 'simple' || res.data === 'advanced') || process.env.DEBUG) {
                                socket.emit('RFID', res.data);
                        };

                        socket.emit('net', true);

                } catch ( err ) {
                        console.log(err);
                        // Se ottengo un errore
                        socket.emit('net', false);
                }
        },

        initRele: () => {
                
        
                // Inizializzo il Gpio
                const rele = new Gpio(PIN, 'out');

                let lastState = null, lastEvents = null;

                try {
                        // Leggo l'ultimo stato e lo applico
                        lastState = fs.readFileSync(path.resolve(__dirname, '../temp/lastState.txt')).toString();
                        switch (lastState) {
                                case 'off':
                                        rele.writeSync(0);
                                        break;

                                default:
                                        rele.writeSync(1);
                                        break;
                        }
                        
                        return { lastState, rele };
                        //console.log(lastEvents);

                } catch(err) {
                        console.log(' [INIT RELE] ', err);
                        lastState = false;
                        return { lastState, rele };
                }
        }
}