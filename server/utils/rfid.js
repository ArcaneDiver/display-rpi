const SerialPort = require('serialport');
const Readline = require('@serialport/parser-readline');
const port = new SerialPort('/dev/ttyUSB0', { baudRate: 9600 });
const e = require('events');

const event = new e.EventEmitter();


var buffer = new Array();
var checksum = new Array();

port.on('data', data =>{
        
        for (const b of data) {
                //console.log(b);
                // 02 => inizio / 03 => fine
                if(b == 2) {

                        buffer = []; 
                        checksum = [];

                } else if (b == 3){

                        if( buffer.length === 14) {
                                
                                buffer.pop();
                                buffer.pop();
                                
                                checksum[1] = buffer.pop();
                                checksum[0] = buffer.pop();

                        } else {
                                
                                //Prende gli ultimi 2 e li mette nel array nel checksum
                                checksum[1] = buffer.pop();
                                checksum[0] = buffer.pop();
                        
                        }
                        for ( var i = 0; i < buffer.length; i++ ) {
                                buffer[i] = String.fromCharCode(buffer[i]);
                        }
                        for ( var i = 0; i < checksum.length; i++ ) {
                                checksum[i] = String.fromCharCode(checksum[i]);
                        }
                        // Calcolo del checksum
                        const sommaChecksum = + ( '0x' + (checksum[0] + checksum[1]) );
                        var calcoloChecksum = 0;
                        
                        for ( var i = 0; i < buffer.length; i += 2 ) {
                                calcoloChecksum = calcoloChecksum ^ +('0x'+(buffer[i] + buffer[i+1]));
                        }
                        
                        if ( calcoloChecksum === sommaChecksum) {
                                

                                event.emit('dataRFID', {
                                        badge: buffer.join(''),
                                        checksum
                                });
                        }
                } else {
                        buffer.push(b);
                }
            
        }
  
});
module.exports = event;