const App = require('express')();

const controller = require('../controllers/react');

App.get('/', controller);

module.exports = App;