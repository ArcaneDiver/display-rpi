const App = require('express')();

const controller = require('../controllers/json');

App.get('/', controller);

module.exports = App;