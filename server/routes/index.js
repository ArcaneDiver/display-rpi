const app = require('express')();

const React = require('./react');
const Json = require('./json');

app.use('/json', Json);
app.use('/', React);

module.exports = app;